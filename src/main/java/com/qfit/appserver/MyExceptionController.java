package com.qfit.appserver;

import com.qfit.appserver.domains.evaluation.exception.AlreadyExistEvaluationRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class MyExceptionController {
    @ExceptionHandler(value = IllegalArgumentException.class)
    public ResponseEntity handleException(Exception exception) {
        return ResponseEntity.badRequest().body(exception.getMessage());
    }

    @ExceptionHandler(value = AlreadyExistEvaluationRequest.class)
    public String getAlreadyExistErrorPage() {
        return "error/alreadyExistEvaluationRequest";
    }

}
