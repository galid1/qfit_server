package com.qfit.appserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class AppserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppserverApplication.class, args);
    }

}
