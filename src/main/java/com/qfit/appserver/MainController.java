package com.qfit.appserver;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.SessionAttribute;

import javax.servlet.http.HttpServletRequest;

@Controller
public class MainController {
    @GetMapping("/")
    public String getMain() {
        return "index";
    }

    @GetMapping("/index")
    public String getIndex() {
        return "index";
    }

    @GetMapping("/main")
    public String getMain(@SessionAttribute("type") String type) {
        if(type.equals("corporation")) {
            return "redirect:/evaluations/result";
        }
        else {
            return "redirect:/evaluations";
        }
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request) {
        request.getSession().invalidate();
        return "index";
    }

    @GetMapping("/policy/{type}")
    public String getPolicy(@PathVariable("type") String type, Model model) {
        if (type.equals("usagePolicy")) {
            model.addAttribute("title", "이용약관");
        }
        else {
            model.addAttribute("title", "개인정보 처리방침");
        }

        return "policy";
    }

    @GetMapping("/ir")
    public String getIr() {
        return "ir";
    }

    @GetMapping("/usage")
    public String getUsage(@SessionAttribute("type") String type,
                           Model model) {
        if(type.equals("corporation")) {
            model.addAttribute("corporationUsage", "/images/corporationUsage.png");
        }
        else {
            model.addAttribute("investorUsage", "/images/investorUsage.png");
        }

        return "usage";
    }

//    @GetMapping("/result")
//    public String getResult(@SessionAttribute("type") String type,
//                            Model model) {
//        if(type.equals("corporation")) {
//            model.addAttribute("corporation", "corporation");
//        }
//        else {
//            model.addAttribute("investor", "investor");
//        }
//
//        return "";
//    }
}
