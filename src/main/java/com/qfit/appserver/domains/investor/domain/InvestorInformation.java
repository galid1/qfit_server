package com.qfit.appserver.domains.investor.domain;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class InvestorInformation {
    @Column(unique = true)
    private String phoneNum;
    private String name;
    private String affiliation;

    @Builder
    public InvestorInformation(String phoneNum, String name, String affiliation) {
        this.phoneNum = phoneNum;
        this.name = name;
        this.affiliation = affiliation;
    }
}
