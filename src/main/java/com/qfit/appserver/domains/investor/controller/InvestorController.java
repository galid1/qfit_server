package com.qfit.appserver.domains.investor.controller;

import com.qfit.appserver.domains.investor.domain.InvestorEntity;
import com.qfit.appserver.domains.investor.service.InvestorAuthService;
import com.qfit.appserver.domains.investor.service.InvestorMyEvaluationService;
import com.qfit.appserver.domains.investor.service.InvestorRegisterRequest;
import com.qfit.appserver.domains.investor.service.InvestorSignInRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@RequestMapping("/investors")
@Controller
public class InvestorController {

    @Autowired
    private InvestorAuthService investorAuthService;
    @Autowired
    private InvestorMyEvaluationService investorMyEvaluationService;

    @PostMapping("/signUp")
    @ResponseBody
    public Long registerInvestor(@RequestBody InvestorRegisterRequest investorRegisterRequest) {
        return investorAuthService.registerInvestor(investorRegisterRequest);
    }

    @PostMapping("/signIn")
    @ResponseBody
    public ResponseEntity signIn(@RequestBody InvestorSignInRequest signInRequest, HttpServletRequest request) throws IOException {
        HttpSession session = request.getSession();
        InvestorEntity investorEntity = investorAuthService.signIn(signInRequest);

        if(investorEntity != null) {
            session.setAttribute("LOGIN", true);
            session.setAttribute("id", investorEntity.getInvestorId());
            session.setAttribute("type", "investor");
            session.setAttribute("investor", "investor");
            session.setAttribute("name", investorEntity.getInvestorInformation().getName());
            return new ResponseEntity(HttpStatus.OK);
        }

        return new ResponseEntity("로그인 정보가 올바르지 않습니다.", HttpStatus.UNAUTHORIZED);
    }



}
