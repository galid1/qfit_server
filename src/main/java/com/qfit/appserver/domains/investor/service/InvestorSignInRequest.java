package com.qfit.appserver.domains.investor.service;

import lombok.*;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class InvestorSignInRequest {
    private String phoneNum;
    private String password;
}
