package com.qfit.appserver.domains.investor.service;

import com.qfit.appserver.domains.evaluation.domain.EvaluationRepository;
import com.qfit.appserver.domains.investor.domain.InvestorEntity;
import com.qfit.appserver.domains.investor.domain.InvestorInformation;
import com.qfit.appserver.domains.investor.domain.InvestorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;

@Service
public class InvestorAuthService {
    @Autowired
    private InvestorRepository investorRepository;

    @Autowired
    private EvaluationRepository evaluationRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    public Long registerInvestor(InvestorRegisterRequest investorRegisterRequest) {
        verifyDuplicatedInvestor(investorRegisterRequest.getInvestorInformation().getPhoneNum());

        InvestorEntity savedEntity = investorRepository.save(InvestorEntity.builder()
                .investorInformation(InvestorInformation.builder()
                        .phoneNum(investorRegisterRequest.getInvestorInformation().getPhoneNum())
                        .affiliation(investorRegisterRequest.getInvestorInformation().getAffiliation())
                        .name(investorRegisterRequest.getInvestorInformation().getName())
                        .build())
                .password(passwordEncoder.encode(investorRegisterRequest.getPassword()))
                .build());

        return savedEntity.getInvestorId();
    }

    private void verifyDuplicatedInvestor(String phoneNum) {
        if(investorRepository.findByInvestorInformation_PhoneNum(phoneNum).isPresent())
            throw new IllegalArgumentException("이미 존재하는 투자자입니다.");
    }

    @Transactional
    public InvestorEntity signIn(InvestorSignInRequest signInRequest) throws HttpClientErrorException.Unauthorized {
        InvestorEntity investorEntity = investorRepository.findByInvestorInformation_PhoneNum(signInRequest.getPhoneNum())
                .orElseThrow(() -> new IllegalArgumentException("존재하지 않는 투자자입니다."));

        if(passwordEncoder.matches(signInRequest.getPassword(), investorEntity.getPassword()))
            return investorEntity;

        return null;
    }
}
