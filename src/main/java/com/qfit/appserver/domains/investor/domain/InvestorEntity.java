package com.qfit.appserver.domains.investor.domain;

import com.qfit.appserver.common.BaseEntity;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "investor")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class InvestorEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long investorId;

    @Embedded
    private InvestorInformation investorInformation;
    private String password;

    @ElementCollection
    @CollectionTable(
            name = "evaluation_id_list",
            joinColumns = @JoinColumn(name = "evaluator")
    )
    @Column(name = "evaluation_id")
    private List<Long> evaluationIdList = new ArrayList<>();

    @Builder
    public InvestorEntity(InvestorInformation investorInformation, String password) {
        this.investorInformation = investorInformation;
        this.password = password;
    }

    public void evaluate(Long evaluationId) {
        if(this.evaluationIdList.contains(evaluationId))
            throw new IllegalArgumentException("이미 평가한 대상입니다.");

        this.evaluationIdList.add(evaluationId);
    }
}

