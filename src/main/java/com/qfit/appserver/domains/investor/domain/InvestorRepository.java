package com.qfit.appserver.domains.investor.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface InvestorRepository extends JpaRepository<InvestorEntity, Long> {
    Optional<InvestorEntity> findByInvestorInformation_PhoneNum(String phoneNum);
}
