package com.qfit.appserver.domains.investor.service;

import lombok.*;

import java.time.LocalDate;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class InvestorMyEvaluationSummary {
    private long evaluationId;
    private String corporationImageUrl;
    private String corporationName;
    private LocalDate evaluationDate;
}
