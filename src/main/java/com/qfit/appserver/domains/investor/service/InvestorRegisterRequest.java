package com.qfit.appserver.domains.investor.service;

import com.qfit.appserver.domains.investor.domain.InvestorInformation;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class InvestorRegisterRequest {
    private InvestorInformation investorInformation;
    private String password;
}
