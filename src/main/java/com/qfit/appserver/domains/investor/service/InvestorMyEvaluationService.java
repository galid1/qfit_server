package com.qfit.appserver.domains.investor.service;

import com.qfit.appserver.domains.evaluation.domain.EvaluationEntity;
import com.qfit.appserver.domains.evaluation.domain.EvaluationRepository;
import com.qfit.appserver.domains.evaluation.domain.EvaluationTargetInformation;
import com.qfit.appserver.domains.investor.domain.InvestorEntity;
import com.qfit.appserver.domains.investor.domain.InvestorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class InvestorMyEvaluationService {
    @Autowired
    private InvestorRepository investorRepository;

    @Autowired
    private EvaluationRepository evaluationRepository;

    @Transactional
    public List<InvestorMyEvaluationSummary> getMyEvaluationSummaries(long investorId) {
        InvestorEntity investorEntity = investorRepository.findById(investorId).orElseThrow(() -> new IllegalArgumentException("존재하지 않는 투자자입니다."));

        return investorEntity.getEvaluationIdList()
                .stream()
                .filter(evaluationId -> evaluationRepository.findById(evaluationId).isPresent())
                .map(evaluationId -> evaluationRepository.findById(evaluationId).get())
                .map(evaluationEntity -> toEvaluationSummary((EvaluationEntity) evaluationEntity))
                .collect(Collectors.toList());
    }

    private InvestorMyEvaluationSummary toEvaluationSummary(EvaluationEntity evaluationEntity) {
        return InvestorMyEvaluationSummary.builder()
                .evaluationId(evaluationEntity.getEvaluationId())
                .corporationImageUrl(evaluationEntity.getCorporationImageUrl())
                .corporationName(evaluationEntity.getEvaluationTargetInformation().getCorporationInformation().getCorporationName())
                .evaluationDate(evaluationEntity.getCreatedDate())
                .build();
    }
}
