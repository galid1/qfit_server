package com.qfit.appserver.domains.corporation.service;

import com.qfit.appserver.domains.corporation.domain.CorporationEntity;
import com.qfit.appserver.domains.corporation.domain.CorporationRepository;
import com.qfit.appserver.domains.corporation.domain.CorporationSelfEvaluateValue;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class CorporationSelfEvaluateService {
    private final CorporationRepository corporationRepository;

    public void selfEvaluate(Long corporationId, CorporationSelfEvaluateRequest request) {
        CorporationEntity corporationEntity = getCorporation(corporationId);
        corporationEntity.selfEvaluate(toCorporationSelfEvaluateValue(request));
    }

    private CorporationEntity getCorporation(Long corporationId) {
        Optional<CorporationEntity> optionalCorporationEntity = corporationRepository.findById(corporationId);

        if(!optionalCorporationEntity.isPresent())
            throw new IllegalArgumentException("존재하지 않는 기업입니다.");

        return optionalCorporationEntity.get();
    }

    private CorporationSelfEvaluateValue toCorporationSelfEvaluateValue(CorporationSelfEvaluateRequest request) {
        return CorporationSelfEvaluateValue.builder()
                .estimatedSales(request.getEstimatedSales())
                .discountRatio(BigDecimal.valueOf(request.getDiscountRatio()))
                .growthRatio(BigDecimal.valueOf(request.getGrowthRatio()))
                .profitRatio(BigDecimal.valueOf(request.getProfitRatio()))
                .build();
    }
}
