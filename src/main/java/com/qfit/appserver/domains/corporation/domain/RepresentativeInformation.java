package com.qfit.appserver.domains.corporation.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RepresentativeInformation {
    private String representativeName;
    @Column(unique = true)
    private String phoneNum;
    private String businessNum;
}
