package com.qfit.appserver.domains.corporation.service;

import lombok.*;

import java.math.BigInteger;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Builder
@Getter
public class CorporationSelfEvaluateRequest {
    private BigInteger estimatedSales;
    private double growthRatio;
    private double profitRatio;
    private double discountRatio;
}
