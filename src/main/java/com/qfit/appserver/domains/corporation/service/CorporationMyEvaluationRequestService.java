package com.qfit.appserver.domains.corporation.service;

import com.qfit.appserver.domains.evaluation.domain.EvaluationEntity;
import com.qfit.appserver.domains.evaluation.domain.EvaluationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CorporationMyEvaluationRequestService {
    @Autowired
    private EvaluationRepository evaluationRepository;

    public EvaluationEntity getMyEvaluationRequest(long corporationId) {
        return evaluationRepository.findByEvaluationTargetInformationEvaluationTargetId(corporationId)
                .orElseThrow(() -> new IllegalArgumentException("존재하지 않는 기업입니다."));
    }
}
