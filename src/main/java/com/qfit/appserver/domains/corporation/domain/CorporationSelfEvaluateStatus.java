package com.qfit.appserver.domains.corporation.domain;

public enum CorporationSelfEvaluateStatus {
    COMPLETE_STATUS, NOT_YET_STATUS
}
