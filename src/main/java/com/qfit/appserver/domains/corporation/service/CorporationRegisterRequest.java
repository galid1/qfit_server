package com.qfit.appserver.domains.corporation.service;

import com.qfit.appserver.domains.corporation.domain.RepresentativeInformation;
import lombok.*;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CorporationRegisterRequest {
    private String corporationName;
    private RepresentativeInformation representativeInformation;
    private String password;
}
