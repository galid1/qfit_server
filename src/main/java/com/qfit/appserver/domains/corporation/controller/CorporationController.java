package com.qfit.appserver.domains.corporation.controller;

import com.qfit.appserver.domains.corporation.domain.CorporationEntity;
import com.qfit.appserver.domains.corporation.domain.CorporationRepository;
import com.qfit.appserver.domains.corporation.domain.CorporationSelfEvaluateValue;
import com.qfit.appserver.domains.corporation.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RequestMapping("/corporations")
@Controller
public class CorporationController {
    @Autowired
    private CorporationRepository corporationRepository;

    @Autowired
    private CorporationAuthService corporationAuthService;

    @Autowired
    private CorporationMyEvaluationRequestService corporationMyEvaluationRequestService;

    @Autowired
    private CorporationSelfEvaluateService corporationSelfEvaluateService;

    @PostMapping("/signUp")
    @ResponseBody
    public Long registerCorporation(@RequestBody CorporationRegisterRequest corporationRegisterRequest) {
        return corporationAuthService.registerCorporation(corporationRegisterRequest);
    }

    @GetMapping("/{corporationId}/evaluation")
    public String getEvaluationRequest(@PathVariable("corporationId") long corporationId, Model model) {
        model.addAttribute("myEvaluationRequest", corporationMyEvaluationRequestService.getMyEvaluationRequest(corporationId));
        return "myEvaluationRequest";
    }

    @PostMapping("/signIn")
    @ResponseBody
    public ResponseEntity signIn(@RequestBody CorporationSignInRequest signInRequest, HttpServletRequest request) {
        HttpSession session = request.getSession();
        CorporationEntity corporationEntity = corporationAuthService.signIn(signInRequest);

        if(corporationEntity != null) {
            session.setAttribute("LOGIN", true);
            session.setAttribute("id", corporationEntity.getCorporationId());
            session.setAttribute("type", "corporation");
            session.setAttribute("corporation", "corporation");
            session.setAttribute("name", corporationEntity.getCorporationName());
            return new ResponseEntity(HttpStatus.OK);
        }

        return new ResponseEntity("로그인 정보가 올바르지 않습니다.", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/self-evaluate")
    public String getSelfEvaluatePage(@SessionAttribute("id") Long id,
                                      Model model) {
        CorporationEntity corporationEntity = corporationRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("존재하지 않는 기업입니다."));

        model.addAttribute("selfEvaluateValue", corporationEntity.getCorporationSelfEvaluateValue());
        model.addAttribute("corporationName", corporationEntity.getCorporationName());
        model.addAttribute("title", "자가기업가치평가");
        return "selfEvaluate";
    }

    @PostMapping("/{corporationId}/self-evaluate")
    public ResponseEntity selfEvaluate(@PathVariable("corporationId") Long corporationId,
                                       @RequestBody CorporationSelfEvaluateRequest request) {
        corporationSelfEvaluateService.selfEvaluate(corporationId, request);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{corporationId}/self-evaluate")
    public ResponseEntity modifySelfEvaluate(@PathVariable("corporationId") Long corporationId,
                                             @RequestBody CorporationSelfEvaluateRequest request) {
        corporationSelfEvaluateService.selfEvaluate(corporationId, request);
        return ResponseEntity.ok().build();
    }

}
