package com.qfit.appserver.domains.corporation.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CorporationSignInRequest {
    private String phoneNum;
    private String password;
}
