package com.qfit.appserver.domains.corporation.domain;

import com.qfit.appserver.common.BaseEntity;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Table(name = "corporation")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CorporationEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long corporationId;

    private String corporationName;
    @Embedded
    private RepresentativeInformation representativeInformation;
    @Embedded
    private CorporationSelfEvaluateValue corporationSelfEvaluateValue = new CorporationSelfEvaluateValue();
    private CorporationSelfEvaluateStatus corporationSelfEvaluateStatus;
    private String password;
    private Long requestEvaluationId;

    @Builder
    public CorporationEntity(RepresentativeInformation representativeInformation, String corporationName, String password) {
        this.representativeInformation = representativeInformation;
        this.corporationName = corporationName;
        this.corporationSelfEvaluateStatus = CorporationSelfEvaluateStatus.NOT_YET_STATUS;
        this.setPassword(password);
    }

    private void setPassword(String password) {
        if (password == null || password.length() < 1)
            throw new IllegalArgumentException("패스워드를 올바르게 입력하세요.");
        this.password = password;
    }

    public void requestEvaluation(Long requestEvaluationId) {
        verifyEvaluationRequest(requestEvaluationId);

        this.requestEvaluationId = requestEvaluationId;
    }

    private void verifyEvaluationRequest(Long requestEvaluationId) {
        if(requestEvaluationId == null)
            throw new IllegalArgumentException("평가 요청 id를 입력하세요.");

        if(this.requestEvaluationId != null)
            throw new IllegalArgumentException("이미 등록된 가치 평가 요청이 존재합니다.");
    }

    public void deleteEvaluationRequest() {
        this.requestEvaluationId = null;
    }

    public void selfEvaluate(CorporationSelfEvaluateValue corporationSelfEvaluateValue) {
        this.corporationSelfEvaluateValue = corporationSelfEvaluateValue;
        this.corporationSelfEvaluateStatus = CorporationSelfEvaluateStatus.COMPLETE_STATUS;
    }

    public CorporationSelfEvaluateValue getCorporationSelfEvaluateValue() {
        if(this.corporationSelfEvaluateStatus != CorporationSelfEvaluateStatus.COMPLETE_STATUS)
            return null;
        else
            return this.corporationSelfEvaluateValue;
    }
}
