package com.qfit.appserver.domains.corporation.domain;

import lombok.*;

import javax.persistence.Embeddable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Embeddable
@Getter
public class CorporationSelfEvaluateValue {
    private BigInteger estimatedSales = BigInteger.ZERO;
    private BigDecimal growthRatio = BigDecimal.ZERO;
    private BigDecimal profitRatio = BigDecimal.ZERO;
    private BigDecimal discountRatio = BigDecimal.ZERO;
    private BigInteger corporationValue = BigInteger.ZERO;

    @Builder
    public CorporationSelfEvaluateValue(BigInteger estimatedSales, BigDecimal growthRatio, BigDecimal profitRatio, BigDecimal discountRatio) {
        setCorporationValueInformation(estimatedSales, growthRatio, profitRatio, discountRatio);
    }

    private void setCorporationValueInformation(BigInteger estimatedSales, BigDecimal growthRatio, BigDecimal profitRatio, BigDecimal discountRatio) {
        this.estimatedSales = estimatedSales;
        this.growthRatio = growthRatio;
        this.profitRatio = profitRatio;
        this.discountRatio = discountRatio;
        this.corporationValue = calculateCorporationValue(estimatedSales, growthRatio, profitRatio, discountRatio);
    }

    private BigInteger calculateCorporationValue(BigInteger estimatedSales, BigDecimal growthRatio, BigDecimal profitRatio, BigDecimal discountRatio) {
        BigDecimal corporationValue = BigDecimal.ZERO;

        BigDecimal decGrowthRatio = BigDecimal.valueOf(growthRatio.intValue()).movePointLeft(2);
        BigDecimal decProfitRatio = BigDecimal.valueOf(profitRatio.intValue()).movePointLeft(2);
        BigDecimal decDiscountRatio = BigDecimal.valueOf(discountRatio.intValue()).movePointLeft(2);

        for (int i = 1; i <= 5; i++) {
            BigDecimal growthRatioPerYear = decGrowthRatio.pow(i - 1);

            BigDecimal molecule = new BigDecimal(estimatedSales)
                    .multiply(growthRatioPerYear)
                    .multiply(decProfitRatio);

            BigDecimal denominator = decDiscountRatio.add(BigDecimal.ONE)
                    .pow(i);

            BigDecimal interimResult = molecule.divide(denominator, 4, RoundingMode.HALF_UP);
            corporationValue = corporationValue.add(interimResult);
        }

        return corporationValue.setScale(0, RoundingMode.HALF_UP).toBigInteger();
    }
}
