package com.qfit.appserver.domains.corporation.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CorporationRepository extends JpaRepository<CorporationEntity, Long> {
    Optional<CorporationEntity> findByRepresentativeInformationPhoneNum(String phoneNum);
}
