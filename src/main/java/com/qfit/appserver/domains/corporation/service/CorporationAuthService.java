package com.qfit.appserver.domains.corporation.service;

import com.qfit.appserver.domains.corporation.domain.CorporationEntity;
import com.qfit.appserver.domains.corporation.domain.CorporationRepository;
import com.qfit.appserver.domains.evaluation.domain.EvaluationEntity;
import com.qfit.appserver.domains.evaluation.domain.EvaluationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;

@Service
public class CorporationAuthService {
    @Autowired
    private CorporationRepository corporationRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    public Long registerCorporation(CorporationRegisterRequest corporationRegisterRequest) {
        verifyDuplicatedUser(corporationRegisterRequest.getRepresentativeInformation().getPhoneNum());

        CorporationEntity entity = CorporationEntity.builder()
                .representativeInformation(corporationRegisterRequest.getRepresentativeInformation())
                .corporationName(corporationRegisterRequest.getCorporationName())
//                .password(passwordEncoder.encode(corporationRegisterRequest.getPassword()))
                .password(corporationRegisterRequest.getPassword())
                .build();

        CorporationEntity savedEntity = corporationRepository.save(entity);

        return savedEntity.getCorporationId();
    }

    private void verifyDuplicatedUser(String phoneNum) {
        if(phoneNum == null || phoneNum.length() < 11)
            throw new IllegalArgumentException("유효하지 않은 핸드폰번호입니다.");

        if(corporationRepository.findByRepresentativeInformationPhoneNum(phoneNum)
                .isPresent())
            throw new IllegalArgumentException("중복된 가입자입니다.");
    }

    @Transactional
    public CorporationEntity signIn(CorporationSignInRequest signInRequest) throws HttpClientErrorException.Unauthorized {
        CorporationEntity corporationEntity = corporationRepository.findByRepresentativeInformationPhoneNum(signInRequest.getPhoneNum())
                .orElseThrow(() -> new IllegalArgumentException("존재하지 않는 기업입니다."));

//        if(passwordEncoder.matches(signInRequest.getPassword(), corporationEntity.getPassword()))
        if(signInRequest.getPassword().equals(corporationEntity.getPassword()))
            return corporationEntity;

        return null;
    }

}
