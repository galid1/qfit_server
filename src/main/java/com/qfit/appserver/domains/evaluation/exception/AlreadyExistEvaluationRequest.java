package com.qfit.appserver.domains.evaluation.exception;

import lombok.Getter;
import org.springframework.stereotype.Component;

@Getter
public class AlreadyExistEvaluationRequest extends Exception{
    public AlreadyExistEvaluationRequest(String message) {
        super(message);
    }
}
