package com.qfit.appserver.domains.evaluation.service;

import lombok.*;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EvaluationRequest {
    private long evaluateTargetId;
    private long evaluatorId;
    private String evaluatorName;
    private int teamCapacity;
    private int marketability;
    private int distinction;
    private int commercializationCapacity;
    private int growth;
    private int financialSoundness;
    private String investorComment;
}
