package com.qfit.appserver.domains.evaluation.service;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UploadFileRequest {
    private List<MultipartFile> fileList;
    private List<String> keyList;
}
