package com.qfit.appserver.domains.evaluation.domain;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CorporationInformation {
    private String corporationName;
    private String corporationIntroduce;
    private String businessNum;
    private String address;
    private String mainProductAndService;
    private String establishmentDate;
    private String event;
    private String businessConditions;

    @Builder
    public CorporationInformation(String corporationName, String corporationIntroduce, String businessNum, String address, String mainProductAndService, String establishmentDate, String event, String businessConditions) {
        this.corporationName = corporationName;
        this.corporationIntroduce = corporationIntroduce;
        this.businessNum = businessNum;
        this.address = address;
        this.mainProductAndService = mainProductAndService;
        this.establishmentDate = establishmentDate;
        this.event = event;
        this.businessConditions = businessConditions;
    }
}
