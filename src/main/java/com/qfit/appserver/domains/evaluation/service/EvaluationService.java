package com.qfit.appserver.domains.evaluation.service;

import com.qfit.appserver.domains.corporation.domain.CorporationEntity;
import com.qfit.appserver.domains.corporation.domain.CorporationRepository;
import com.qfit.appserver.domains.evaluation.domain.EvaluationEntity;
import com.qfit.appserver.domains.evaluation.domain.EvaluationInformation;
import com.qfit.appserver.domains.evaluation.domain.EvaluationRepository;
import com.qfit.appserver.domains.evaluation.domain.EvaluationTargetInformation;
import com.qfit.appserver.domains.investor.domain.InvestorEntity;
import com.qfit.appserver.domains.investor.domain.InvestorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EvaluationService {
    @Autowired
    private EvaluationRepository evaluationRepository;
    @Autowired
    private InvestorRepository investorRepository;
    @Autowired
    private CorporationRepository corporationRepository;

    @Transactional
    public void evaluate(EvaluationRequest evaluationRequest) {
        EvaluationEntity evaluationTarget = evaluationRepository.findById(evaluationRequest.getEvaluateTargetId())
                .orElseThrow(() -> new IllegalArgumentException("존재하지 않는 평가대상입니다."));

        InvestorEntity evaluator = investorRepository.findById(evaluationRequest.getEvaluatorId())
                .orElseThrow(() -> new IllegalArgumentException("존재하지 않는 평가자입니다."));


        evaluationTarget.evaluate(toEvaluationInformation(evaluationRequest));
        evaluator.evaluate(evaluationRequest.getEvaluateTargetId());
    }

    private EvaluationInformation toEvaluationInformation(EvaluationRequest evaluationRequest) {
        return EvaluationInformation.builder()
                .evaluatorId(evaluationRequest.getEvaluatorId())
                .evaluatorName(evaluationRequest.getEvaluatorName())
                .teamCapacity(evaluationRequest.getTeamCapacity())
                .marketability(evaluationRequest.getMarketability())
                .growth(evaluationRequest.getGrowth())
                .distinction(evaluationRequest.getDistinction())
                .financialSoundness(evaluationRequest.getFinancialSoundness())
                .commercializationCapacity(evaluationRequest.getCommercializationCapacity())
                .investorComment(evaluationRequest.getInvestorComment())
                .build();
    }

    public List<EvaluationSummary> getEvaluationList() {
        return evaluationRepository.findAll()
                .stream()
                .map(evaluationEntity -> toEvaluationSummary(evaluationEntity))
                .collect(Collectors.toList());
    }

    private EvaluationSummary toEvaluationSummary(EvaluationEntity evaluationEntity) {
        return EvaluationSummary.builder()
                .evaluationId(evaluationEntity.getEvaluationId())
                .corporationImageUrl(evaluationEntity.getCorporationImageUrl())
                .corporationIntroduce(evaluationEntity.getEvaluationTargetInformation().getCorporationInformation().getCorporationIntroduce())
                .corporationName(evaluationEntity.getEvaluationTargetInformation().getCorporationInformation().getCorporationName())
                .build();
    }

    @Transactional
    public EvaluationEntity getEvaluationByEvaluationId(long evaluationId) {
        EvaluationEntity evaluationEntity = evaluationRepository.findById(evaluationId).orElseThrow(() -> new IllegalArgumentException("존재하지 않는 평가입니다."));
        return evaluationEntity;
    }

    public Optional<EvaluationEntity> getEvaluationByCorporationId(long requesterId) {
        Optional<EvaluationEntity> evaluationEntity = evaluationRepository.findByEvaluationTargetInformationEvaluationTargetId(requesterId);
        return evaluationEntity;
    }

    public List<EvaluationInformation> getEvaluationInformationList(long evaluationId) {
        EvaluationEntity evaluationEntity = evaluationRepository.findById(evaluationId).orElseThrow(() -> new IllegalArgumentException("존재하지 않는 평가입니다."));
        return evaluationEntity.getEvaluationInformationList();
    }

    @Transactional
    public void modifyEvaluation(long evaluationId, EvaluationModifyRequest modifyRequest) {
        EvaluationEntity evaluationEntity = evaluationRepository.findByEvaluationTargetInformationEvaluationTargetId(evaluationId)
                .orElseThrow(() -> new IllegalArgumentException("존재하지 않는 가치평가 요청입니다."));

        evaluationEntity.modifyEvaluation(modifyRequest);
    }

    @Transactional
    public void deleteEvaluation(long corporationId, long evaluationId) {
        CorporationEntity corporationEntity = corporationRepository.findById(corporationId)
                .orElseThrow(() -> new IllegalArgumentException("존재하지 않는 기업이거나, 로그인이 되지 않은 상태입니다."));
        corporationEntity.deleteEvaluationRequest();

        evaluationRepository.deleteById(evaluationId);
    }
}
