package com.qfit.appserver.domains.evaluation.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

@Embeddable
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EvaluationTargetInformation {
    @Column(unique = true)
    private long evaluationTargetId;

    @Embedded
    private CorporationInformation corporationInformation;
    @Embedded
    private RepresentativeInformation representativeInformation;
    @Embedded
    private FinanceInformation financeInformation;


    @Builder
    public EvaluationTargetInformation(long evaluationTargetId,
                                    CorporationInformation corporationInformation,
                                    RepresentativeInformation representativeInformation,
                                    FinanceInformation financeInformation) {
        this.evaluationTargetId = evaluationTargetId;
        this.corporationInformation = corporationInformation;
        this.representativeInformation = representativeInformation;
        this.financeInformation = financeInformation;
    }

}
