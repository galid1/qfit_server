package com.qfit.appserver.domains.evaluation.service;

import lombok.*;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EvaluationSummary {
    private long evaluationId;
    private String corporationName;
    private String corporationIntroduce;
    private String corporationImageUrl;
}
