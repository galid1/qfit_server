package com.qfit.appserver.domains.evaluation.service;

import com.qfit.appserver.common.file.FileUtil;
import com.qfit.appserver.domains.corporation.domain.CorporationEntity;
import com.qfit.appserver.domains.corporation.domain.CorporationRepository;
import com.qfit.appserver.domains.evaluation.domain.EvaluationEntity;
import com.qfit.appserver.domains.evaluation.domain.EvaluationRepository;
import com.qfit.appserver.domains.evaluation.domain.EvaluationTargetInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class EvaluationRegisterService {
    @Autowired
    private EvaluationRepository evaluationRepository;
    @Autowired
    private CorporationRepository corporationRepository;

    @Autowired
    private FileUtil fileUtil;

    @Transactional
    public Long registerEvaluation(long requesterId, EvaluationRegisterRequest evaluationRegisterRequest) {
        EvaluationEntity registeredEvaluation = requestEvaluation(requesterId, evaluationRegisterRequest);
        return registeredEvaluation.getEvaluationId();
    }

    @Transactional
    public void uploadImageList(long requesterId, Map<String, MultipartFile> fileMap) {
        EvaluationEntity evaluationEntity = evaluationRepository.findByEvaluationTargetInformationEvaluationTargetId(requesterId).get();
        uploadFiles(evaluationEntity, fileMap);
    }

    private EvaluationEntity requestEvaluation(long requesterId, EvaluationRegisterRequest evaluationRegisterRequest) {
        EvaluationEntity evaluation = evaluationRepository.save(EvaluationEntity.builder()
                .evaluationTargetInformation(toEvaluationTargetInformation(requesterId, evaluationRegisterRequest))
                .password(evaluationRegisterRequest.getPassword())
                .build());

        CorporationEntity requestor = corporationRepository
                .findById(requesterId)
                .orElseThrow(() -> new IllegalArgumentException("존재하지 않는 기업입니다."));

        requestor.requestEvaluation(evaluation.getEvaluationId());

        return evaluation;
    }

    private EvaluationTargetInformation toEvaluationTargetInformation(long requesterId, EvaluationRegisterRequest evaluationRegisterRequest) {
        return EvaluationTargetInformation.builder()
                .evaluationTargetId(requesterId)
                .representativeInformation(evaluationRegisterRequest.getRepresentativeInformation())
                .financeInformation(evaluationRegisterRequest.getFinanceInformation())
                .corporationInformation(evaluationRegisterRequest.getCorporationInformation())
                .build();
    }

    private void uploadFiles(EvaluationEntity evaluation, Map<String, MultipartFile> fileMap) {
        Map<String, String> fileUrlMap = fileMap.entrySet()
                .stream()
                .filter(entry -> entry.getValue()!= null)
                .map(entry -> {
                    String uploadedUrl = fileUtil.uploadFile(entry.getValue());
                    return new AbstractMap.SimpleEntry<>(entry.getKey(), uploadedUrl);
                })
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        evaluation.uploadFiles(fileUrlMap);
    }

}
