package com.qfit.appserver.domains.evaluation.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EvaluationRepository extends JpaRepository<EvaluationEntity, Long> {
    Optional<EvaluationEntity> findByEvaluationTargetInformationEvaluationTargetId(long evaluationTargetId);
}
