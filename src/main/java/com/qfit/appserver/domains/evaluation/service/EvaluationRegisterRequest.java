package com.qfit.appserver.domains.evaluation.service;

import com.qfit.appserver.domains.evaluation.domain.RepresentativeInformation;
import com.qfit.appserver.domains.evaluation.domain.CorporationInformation;
import com.qfit.appserver.domains.evaluation.domain.FinanceInformation;
import lombok.*;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EvaluationRegisterRequest {
    private CorporationInformation corporationInformation;
    private RepresentativeInformation representativeInformation;
    private FinanceInformation financeInformation;
    private String password;
}
