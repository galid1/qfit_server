package com.qfit.appserver.domains.evaluation.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RepresentativeInformation {
    private String representativeName;
    private String gender;
    private String birthDay;
    private String position;
    private String academyBackground;
    private String major;
    private String career;
    private String qualification;
}