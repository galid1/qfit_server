package com.qfit.appserver.domains.evaluation.domain;

import lombok.*;

import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@AllArgsConstructor
public class EvaluationFileInformation {
    private String irFileUrl;
    private String financeChartFileUrl;
    private String incomeFileUrl;
    private String businessRegistrationFileUrl;
    private String associationFileUrl;
    private String patentFileUrl;
}
