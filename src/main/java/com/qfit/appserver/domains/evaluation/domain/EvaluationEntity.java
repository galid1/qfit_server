package com.qfit.appserver.domains.evaluation.domain;

import com.qfit.appserver.common.BaseEntity;
import com.qfit.appserver.domains.evaluation.service.EvaluationModifyRequest;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Entity
@Table(name = "evaluation")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EvaluationEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long evaluationId;

    private String corporationImageUrl;

    @Embedded
    private EvaluationTargetInformation evaluationTargetInformation;

    @Embedded
    private EvaluationFileInformation evaluationFileInformation;

    @ElementCollection
    @CollectionTable(
            name = "evaluator_id_list",
            joinColumns = @JoinColumn(name = "evaluation_id")
    )
    private List<EvaluationInformation> evaluationInformationList = new ArrayList<>();

    private String password;

    @Builder
    public EvaluationEntity(EvaluationTargetInformation evaluationTargetInformation,
                            String password) {
        this.setEvaluationTargetInformation(evaluationTargetInformation);
        this.setPassword(password);
    }

    private void setEvaluationTargetInformation(EvaluationTargetInformation evaluationTargetInformation) {
        if(evaluationTargetInformation == null)
            throw new IllegalArgumentException("평가대상 정보를 올바르게 입력하세요.");

        this.evaluationTargetInformation = evaluationTargetInformation;
    }

    private void setPassword(String password) {
        if(password == null || password.length() < 1)
            throw new IllegalArgumentException("비밀번호를 올바르게 입력하세요.");

        this.password = password;
    }

    public void evaluate(EvaluationInformation newEvaluationInformation) {
        this.verifyAlreadyEvaluateInvestor(newEvaluationInformation);
        this.evaluationInformationList.add(newEvaluationInformation);
    }

    private void verifyAlreadyEvaluateInvestor(EvaluationInformation newEvaluationInformation) {
        if(this.evaluationInformationList
                .stream()
                .map(evaluationInformation -> evaluationInformation.getEvaluatorId())
                .collect(Collectors.toList())
                .contains(newEvaluationInformation.getEvaluatorId()))
            throw new IllegalArgumentException("이미 평가를 진행한 평가자입니다.");
    }

    public void uploadFiles(Map<String, String> fileMap) {
        this.corporationImageUrl = fileMap.getOrDefault("corporationImage", this.corporationImageUrl);

        if(evaluationFileInformation == null)
            this.evaluationFileInformation = EvaluationFileInformation.builder()
                .associationFileUrl(fileMap.getOrDefault("association", ""))
                .businessRegistrationFileUrl(fileMap.getOrDefault("businessRegistration", ""))
                .financeChartFileUrl(fileMap.getOrDefault("financeChart", ""))
                .patentFileUrl(fileMap.getOrDefault("patent", ""))
                .incomeFileUrl(fileMap.getOrDefault("income", ""))
                .irFileUrl(fileMap.getOrDefault("ir", ""))
                    .build();
        else
            this.evaluationFileInformation = EvaluationFileInformation.builder()
                .associationFileUrl(fileMap.getOrDefault("association", evaluationFileInformation.getAssociationFileUrl()))
                .businessRegistrationFileUrl(fileMap.getOrDefault("businessRegistration", evaluationFileInformation.getBusinessRegistrationFileUrl()))
                .financeChartFileUrl(fileMap.getOrDefault("financeChart", evaluationFileInformation.getFinanceChartFileUrl()))
                .patentFileUrl(fileMap.getOrDefault("patent", evaluationFileInformation.getPatentFileUrl()))
                .incomeFileUrl(fileMap.getOrDefault("income", evaluationFileInformation.getIncomeFileUrl()))
                .irFileUrl(fileMap.getOrDefault("ir", evaluationFileInformation.getIrFileUrl()))
                .build();
    }

    public void modifyEvaluation(EvaluationModifyRequest modifyRequest) {
        this.setEvaluationTargetInformation(EvaluationTargetInformation.builder()
                .evaluationTargetId(this.evaluationTargetInformation.getEvaluationTargetId())
                .representativeInformation(modifyRequest.getRepresentativeInformation())
                .financeInformation(modifyRequest.getFinanceInformation())
                .corporationInformation(modifyRequest.getCorporationInformation())
                .build());

        this.setPassword(modifyRequest.getPassword());
    }
}
