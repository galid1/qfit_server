package com.qfit.appserver.domains.evaluation.controller;

import com.qfit.appserver.domains.corporation.domain.CorporationEntity;
import com.qfit.appserver.domains.corporation.domain.CorporationRepository;
import com.qfit.appserver.domains.evaluation.domain.EvaluationEntity;
import com.qfit.appserver.domains.evaluation.domain.EvaluationFileInformation;
import com.qfit.appserver.domains.evaluation.domain.EvaluationRepository;
import com.qfit.appserver.domains.evaluation.exception.AlreadyExistEvaluationRequest;
import com.qfit.appserver.domains.evaluation.service.*;
import com.qfit.appserver.domains.investor.domain.InvestorEntity;
import com.qfit.appserver.domains.investor.domain.InvestorRepository;
import com.qfit.appserver.domains.investor.service.InvestorMyEvaluationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;
import java.util.Optional;

@RequestMapping("/evaluations")
@Controller
public class EvaluationController {
    @Autowired
    private EvaluationRepository evaluationRepository;
    @Autowired
    private EvaluationRegisterService evaluationRegisterService;

    @Autowired
    private EvaluationService evaluationService;

    @Autowired
    private InvestorMyEvaluationService investorMyEvaluationService;

    @Autowired
    private CorporationRepository corporationRepository;
    @Autowired
    private InvestorRepository investorRepository;

    @GetMapping("/result")
    public String getResult(@SessionAttribute("type") String type,
                            @SessionAttribute("id") long id,
                            Model model) {
        if(type.equals("corporation")) {
            CorporationEntity corporationEntity = corporationRepository.findById(id).get();
            Optional<EvaluationEntity> evaluation = evaluationService.getEvaluationByCorporationId(id);

            if(evaluation.isPresent()) {
                EvaluationEntity evaluationEntity = evaluation.get();
                model.addAttribute("corporationImageUrl", evaluationEntity.getCorporationImageUrl());
                model.addAttribute("evaluationId", evaluationEntity.getEvaluationId());
                model.addAttribute("createdDate", evaluationEntity.getCreatedDate());
                model.addAttribute("mainProductService", evaluationEntity.getEvaluationTargetInformation().getCorporationInformation().getMainProductAndService());
            }

            model.addAttribute("phone", corporationEntity.getRepresentativeInformation().getPhoneNum());
            model.addAttribute("representativeName", corporationEntity.getRepresentativeInformation().getRepresentativeName());
            model.addAttribute("corporationName", corporationEntity.getCorporationName());
            model.addAttribute("businessNum", corporationEntity.getRepresentativeInformation().getBusinessNum());
            return "myEvaluationRequest";
        }
        else {
            InvestorEntity investorEntity = investorRepository.findById(id).get();
            model.addAttribute("phone", investorEntity.getInvestorInformation().getPhoneNum());
            model.addAttribute("affiliation", investorEntity.getInvestorInformation().getAffiliation());
            model.addAttribute("myEvaluationList", investorMyEvaluationService.getMyEvaluationSummaries(id));
            return "myEvaluationList";
        }
    }

    @GetMapping("/register")
    public String getEvaluationRegistry(@SessionAttribute("id") long id,
                                        @SessionAttribute("type") String type) throws AlreadyExistEvaluationRequest {

        if(type.equals("corporation")) {
            CorporationEntity corporationEntity = corporationRepository.findById(id).get();
            if(corporationEntity.getRequestEvaluationId() != null)
                throw new AlreadyExistEvaluationRequest("이미 요청한 평가가 존재합니다.");

            return "registerEvaluationStep";
        }
        else
            return "redirect:/evaluations";
    }

    @PostMapping("/register")
    @ResponseBody
    public Long registerCorporationEvaluation(@SessionAttribute("id") long requesterId,
                                              @RequestBody EvaluationRegisterRequest evaluationRegisterRequest) {
        return evaluationRegisterService.registerEvaluation(requesterId, evaluationRegisterRequest);
    }

    @PostMapping("/register/images")
    public void uploadImageList(@SessionAttribute("id")long requesterId, @RequestParam Map<String, MultipartFile> fileMap) {
        evaluationRegisterService.uploadImageList(requesterId, fileMap);
    }

    // TODO
    @GetMapping
    public String getEvaluationRequestList(Model model) {
        model.addAttribute("evaluationList", evaluationService.getEvaluationList());
        return "evaluationList";
    }

    @GetMapping("/myRequest")
    public String getEvaluation(@SessionAttribute("id") long requesterId,
                                Model model) {
        Optional<EvaluationEntity> evaluation = evaluationService.getEvaluationByCorporationId(requesterId);

        
        model.addAttribute("evaluation", evaluation);
        model.addAttribute("title", "큐핏평가플랫폼");

        if(evaluation.isPresent()) {
            EvaluationEntity evaluationEntity = evaluation.get();
            model.addAttribute("corporationImageUrl", evaluationEntity.getCorporationImageUrl());
            model.addAttribute("evaluationInformationList", evaluationService.getEvaluationInformationList(evaluationEntity.getEvaluationId()));
            model.addAttribute("count", evaluationService.getEvaluationInformationList(evaluationEntity.getEvaluationId()).size());
        }
        return "myEvaluationResult";
    }


//   investor 관련
    @GetMapping("/{evaluationId}")
    public String getEvaluationDetail(@PathVariable("evaluationId") long evaluationId,
                                      @SessionAttribute("id") long evaluatorId,
                                      Model model) {
        EvaluationEntity evaluation = evaluationService.getEvaluationByEvaluationId(evaluationId);
        model.addAttribute("evaluatorId", evaluatorId);
        model.addAttribute("evaluationId", evaluation.getEvaluationId());
        model.addAttribute("password", evaluation.getPassword());
        
        model.addAttribute("corporationImageUrl", evaluation.getCorporationImageUrl());
        model.addAttribute("title", evaluation.getEvaluationTargetInformation().getCorporationInformation().getCorporationName());
        model.addAttribute("corporationInformation", evaluation.getEvaluationTargetInformation().getCorporationInformation());
        model.addAttribute("representativeInformation", evaluation.getEvaluationTargetInformation().getRepresentativeInformation());
        model.addAttribute("financeInformation", evaluation.getEvaluationTargetInformation().getFinanceInformation());

        EvaluationFileInformation evaluationFileInformation = evaluation.getEvaluationFileInformation();
        evaluationFileInformation = getEvaluationFileInformationOrDefault(evaluationFileInformation);
        model.addAttribute("fileInformation", evaluationFileInformation);
        return "detailEvaluation";
    }

    private EvaluationFileInformation getEvaluationFileInformationOrDefault(EvaluationFileInformation evaluationFileInformation) {
        if (evaluationFileInformation == null)
            return EvaluationFileInformation.builder().build();

        return EvaluationFileInformation.builder()
                .associationFileUrl(getValueOrNull(evaluationFileInformation.getAssociationFileUrl()))
                .businessRegistrationFileUrl(getValueOrNull(evaluationFileInformation.getBusinessRegistrationFileUrl()))
                .financeChartFileUrl(getValueOrNull(evaluationFileInformation.getFinanceChartFileUrl()))
                .incomeFileUrl(getValueOrNull(evaluationFileInformation.getIncomeFileUrl()))
                .irFileUrl(getValueOrNull(evaluationFileInformation.getIrFileUrl()))
                .patentFileUrl(getValueOrNull(evaluationFileInformation.getPatentFileUrl()))
                .build();
    }

    private String getValueOrNull(String value) {
        if(value == null)
            return null;

        if(value.length() == 0)
            return null;
        return value;
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity evaluate(@RequestBody EvaluationRequest evaluationRequest) {
        evaluationService.evaluate(evaluationRequest);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{evaluationId}/modify")
    public String getEvaluationModifyPage(@PathVariable("evaluationId") long evaluationId,
                                          Model model) {
        EvaluationEntity evaluationEntity = evaluationRepository.findById(evaluationId)
                .orElseThrow(() -> new IllegalArgumentException("존재하지 않는 가치평가요청입니다."));
        model.addAttribute("evaluation", evaluationEntity);

        EvaluationFileInformation evaluationFileInformation = getEvaluationFileInformation(evaluationEntity);
        model.addAttribute("fileInformationList", evaluationFileInformation);

        return "modifyEvaluation";
    }

    private EvaluationFileInformation getEvaluationFileInformation(EvaluationEntity evaluationEntity) {
        if(evaluationEntity.getEvaluationFileInformation() == null)
            return EvaluationFileInformation.builder()
                    .build();

        return EvaluationFileInformation.builder()
                .patentFileUrl(getFileName(evaluationEntity.getEvaluationFileInformation().getPatentFileUrl()))
                .irFileUrl(getFileName(evaluationEntity.getEvaluationFileInformation().getIrFileUrl()))
                .incomeFileUrl(getFileName(evaluationEntity.getEvaluationFileInformation().getIncomeFileUrl()))
                .financeChartFileUrl(getFileName(evaluationEntity.getEvaluationFileInformation().getFinanceChartFileUrl()))
                .businessRegistrationFileUrl(getFileName(evaluationEntity.getEvaluationFileInformation().getBusinessRegistrationFileUrl()))
                .associationFileUrl(getFileName(evaluationEntity.getEvaluationFileInformation().getAssociationFileUrl()))
                .build();
    }

    private String getFileName(String fileUrl) {
        if(fileUrl == null)
            return "";

        String[] split = fileUrl.split("/");
        return split[split.length - 1];
    }

    @PutMapping("/{evaluationId}")
    public ResponseEntity modifyEvaluation(@PathVariable("evaluationId") long evaluationId,
                                           @RequestBody EvaluationModifyRequest modifyRequest) {
        evaluationService.modifyEvaluation(evaluationId, modifyRequest);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{evaluationId}")
    public ResponseEntity deleteEvaluation(@SessionAttribute("id") long corporationId,
                                           @PathVariable("evaluationId")long evaluationId) {
        evaluationService.deleteEvaluation(corporationId, evaluationId);
        return ResponseEntity.ok().build();
    }
}
