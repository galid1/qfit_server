package com.qfit.appserver.domains.evaluation.domain;

import lombok.*;

import javax.persistence.Embeddable;
import java.math.BigInteger;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Embeddable
public class FinanceInformation {
    private String fiscalYear;
    private String assets;
    private String debt;
    private String capital;      // 자본금
    private String negativeDebt; // 자본
    private String sales;
    private String businessProfits;
    private String netProfits;
}
