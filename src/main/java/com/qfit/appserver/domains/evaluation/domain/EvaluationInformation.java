package com.qfit.appserver.domains.evaluation.domain;

import lombok.*;

import javax.persistence.Embeddable;
import java.util.Arrays;
import java.util.List;

@Embeddable
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class EvaluationInformation {
    private long evaluatorId;
    private String evaluatorName;
    private int teamCapacity;
    private int marketability;
    private int distinction;
    private int commercializationCapacity;
    private int growth;
    private int financialSoundness;
    private String investorComment;

    public EvaluationInformation(long evaluatorId,
                                 String evaluatorName,
                                 int teamCapacity,
                                 int marketability,
                                 int distinction,
                                 int commercializationCapacity,
                                 int growth,
                                 int financialSoundness,
                                 String investorComment) {
        this.verifyEvaluationScoreList(Arrays.asList(teamCapacity,
                marketability,
                distinction,
                commercializationCapacity,
                growth,
                financialSoundness));
        this.evaluatorId = evaluatorId;
        this.evaluatorName = evaluatorName;
        this.teamCapacity = teamCapacity;
        this.marketability = marketability;
        this.distinction = distinction;
        this.commercializationCapacity = commercializationCapacity;
        this.growth = growth;
        this.financialSoundness = financialSoundness;
        this.investorComment = investorComment;
    }

    private void verifyEvaluationScoreList(List<Integer> evaluationScoreList) {
        evaluationScoreList.forEach(score -> {
            if (score < 0 || score > 10)
                throw new IllegalArgumentException("각 평가점수는 0점 ~ 10점 사이어야 합니다.");
        });
    }
}
