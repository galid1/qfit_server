package com.qfit.appserver.domains.evaluation.service;

import com.qfit.appserver.domains.evaluation.domain.CorporationInformation;
import com.qfit.appserver.domains.evaluation.domain.FinanceInformation;
import com.qfit.appserver.domains.evaluation.domain.RepresentativeInformation;
import lombok.*;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EvaluationModifyRequest {
    private CorporationInformation corporationInformation;
    private RepresentativeInformation representativeInformation;
    private FinanceInformation financeInformation;
    private String password;
}
