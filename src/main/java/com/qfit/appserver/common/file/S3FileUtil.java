package com.qfit.appserver.common.file;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.qfit.appserver.common.aws.SimpleS3Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@Component
@Primary
public class S3FileUtil implements FileUtil{
    @Autowired
    private SimpleS3Client simpleS3Client;

    private String bucketName = "qfit";
    private String fileName = "";

    @Override
    public String uploadFile(MultipartFile multipartFile) {
        this.setFileName(multipartFile.getOriginalFilename());

        simpleS3Client.getS3Client(Regions.AP_NORTHEAST_2)
                .putObject(this.makePublicPutObjectRequest(multipartFile));

        return this.getUploadedFileUrl();
    }

    private void setFileName(String fileName) {
        this.fileName = fileName;
    }

    private PutObjectRequest makePublicPutObjectRequest(MultipartFile multipartFile) {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType(multipartFile.getContentType());
        objectMetadata.setContentLength(multipartFile.getSize());

        try {
            return new PutObjectRequest(this.bucketName, this.fileName, multipartFile.getInputStream(), objectMetadata)
                            .withCannedAcl(CannedAccessControlList.PublicRead);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalStateException("업로드에 실패했습니다.");
        }
    }

    private String getUploadedFileUrl() {
        return simpleS3Client.getS3Client(Regions.AP_NORTHEAST_2)
                .getResourceUrl(this.bucketName, this.fileName);
    }

}
