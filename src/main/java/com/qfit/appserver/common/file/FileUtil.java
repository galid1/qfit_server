package com.qfit.appserver.common.file;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;

public interface FileUtil {
    default String uploadFile(File file) {
      throw new UnsupportedOperationException("아직 지원되지 않습니다.");
    }

    default String uploadFile(InputStream inputStream) {
        throw new UnsupportedOperationException("아직 지원되지 않습니다.");
    }

    default String uploadFile(MultipartFile multipartFile) {
        throw new UnsupportedOperationException("아직 지원되지 않습니다.");
    }
}
