package com.qfit.appserver.common.config;

import com.qfit.appserver.domains.corporation.domain.CorporationEntity;
import com.qfit.appserver.domains.corporation.domain.CorporationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

// TODO
@Component
public class LoginFilter implements Filter {
    private List<String> excludePathList = Arrays.asList("/", "/index", "/signIn", "/signUp", "/corporations/signUp", "/corporations/signIn", "/investors/signUp", "/investors/signIn");

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse res = (HttpServletResponse)response;

        String requestPath = req.getServletPath();
        HttpSession session = req.getSession();

        if (requestPath.equals("/policy/usagePolicy") || requestPath.equals("/policy/privacyPolicy")) {
            chain.doFilter(request, response);
            return;
        }

        if (requestPath.startsWith("/css") || requestPath.startsWith("/images")){
            chain.doFilter(request, response);
            return;
        }

        if (excludePathList.contains(requestPath)) {
            chain.doFilter(request, response);
            return;
        }
        else if(!excludePathList.contains(requestPath) && req.getSession().getAttribute("LOGIN") == null) {
            res.sendRedirect("/signIn");
            return;
        }

        chain.doFilter(request, response);
    }

}
