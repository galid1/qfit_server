package com.qfit.appserver.domains.investor.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class InvestorRepositoryTest {
    @Autowired
    private InvestorRepository investorRepository;

    private InvestorEntity investorEntity;

    @Before
    public void init() {
        InvestorInformation investorInformation = InvestorInformation.builder()
                .affiliation("SUCCESS")
                .name("JJY")
                .phoneNum("0000")
                .build();

        this.investorEntity = InvestorEntity.builder()
                .investorInformation(investorInformation)
                .password("1234")
                .build();
    }

    @Test
    public void saveInvestorEntity() {

    }

    @Test(expected = IllegalArgumentException.class)
    public void whenAddDuplicatedEvaluationThenThrowException() {
        investorEntity.evaluate(1l);
        investorEntity.evaluate(1l);
    }
}
