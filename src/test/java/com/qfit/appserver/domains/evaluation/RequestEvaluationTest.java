package com.qfit.appserver.domains.evaluation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qfit.appserver.domains.evaluation.domain.*;
import com.qfit.appserver.domains.evaluation.service.EvaluationRegisterRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class RequestEvaluationTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objMapper;

    private EvaluationRegisterRequest request;

    @Before
    public void init() {
        EvaluationFileInformation imageUrlInformation = EvaluationFileInformation.builder()
                .build();

        CorporationInformation corporationInformation = CorporationInformation.builder()
                .build();

        FinanceInformation financeInformation = FinanceInformation.builder()
                .build();

        RepresentativeInformation representativeInformation = RepresentativeInformation.builder()
                .build();

        request = EvaluationRegisterRequest.builder()
            .password("1234")
            .build();
    }

    @Test
    public void 평가요청테스트() throws Exception {
        mockMvc.perform(post("/evaluations")
                    .content(objMapper.writeValueAsString(request))
                    .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
