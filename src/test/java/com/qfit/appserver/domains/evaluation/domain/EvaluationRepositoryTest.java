package com.qfit.appserver.domains.evaluation.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EvaluationRepositoryTest {
    @Autowired
    private EvaluationRepository evaluationRepository;

    private EvaluationEntity evaluationEntity;

    @Before
    public void init() {
        EvaluationTargetInformation evaluationTargetInformation = EvaluationTargetInformation.builder()
                .build();

        this.evaluationEntity = EvaluationEntity.builder()
                .evaluationTargetInformation(evaluationTargetInformation)
                .password("1234")
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenDuplicatedEvaluatorThenThrowException() {
        //given
        int correctScore = 5;

        EvaluationInformation evaluationInformation = EvaluationInformation.builder()
                .evaluatorId(1l)
                .commercializationCapacity(correctScore)
                .financialSoundness(correctScore)
                .distinction(correctScore)
                .evaluatorId(correctScore)
                .growth(correctScore)
                .marketability(correctScore)
                .teamCapacity(correctScore)
                .build();

        // when
        evaluationEntity.evaluate(evaluationInformation);
        evaluationEntity.evaluate(evaluationInformation);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenGivenInCorrectEvaluationScoreThenThrowException() {
        int inCorrectScore = -1;

        EvaluationInformation inCorrectEvaluationInformation = EvaluationInformation.builder()
                .evaluatorId(1l)
                .commercializationCapacity(inCorrectScore)
                .financialSoundness(inCorrectScore)
                .distinction(inCorrectScore)
                .evaluatorId(inCorrectScore)
                .growth(inCorrectScore)
                .marketability(inCorrectScore)
                .teamCapacity(inCorrectScore)
                .build();

        // when
        this.evaluationEntity.evaluate(inCorrectEvaluationInformation);
    }
}
