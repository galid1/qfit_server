package com.qfit.appserver.domains.corporation.domain;

import com.qfit.appserver.domains.evaluation.domain.EvaluationEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CorporationRepositoryTest {
    @Autowired
    private CorporationRepository corporationRepository;

    private CorporationEntity entity;

    @Before
    public void initBeforeRunTest() {
        // given
        RepresentativeInformation representativeInformation = RepresentativeInformation.builder()
                .businessNum("1111")
                .phoneNum("1111")
                .representativeName("JJY")
                .build();

        entity = CorporationEntity.builder()
                .corporationName("SUCCESS")
                .representativeInformation(representativeInformation)
                .password("1234")
                .build();
    }

    @Test
    public void saveCorporationTest() {
        // when
        CorporationEntity savedEntity = corporationRepository.save(entity);

        // then
        assertThat(savedEntity.getCorporationName(), is(not(equalTo(null))));
        assertThat(savedEntity.getRepresentativeInformation(), is(not(equalTo(null))));
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenAlreadyExistEvaluationRequestThenThrowException() {
        long mockEvaluationId = 1l;
        entity.requestEvaluation(mockEvaluationId);
        entity.requestEvaluation(mockEvaluationId);
    }

}
