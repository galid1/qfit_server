package com.qfit.appserver.domains.corporation.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qfit.appserver.domains.corporation.domain.CorporationRepository;
import com.qfit.appserver.domains.corporation.service.CorporationRegisterRequest;
import com.qfit.appserver.domains.corporation.service.CorporationAuthService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class CorporationControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objMapper;

    @MockBean
    private CorporationAuthService service;
    @MockBean
    private CorporationRepository repository;

    private CorporationRegisterRequest request;

    @Before
    public void init() {
        this.request = CorporationRegisterRequest.builder()
                .password("1234")
                .build();
    }

    @Test
    public void whenRegisterThenReturnId() throws Exception {
        mockMvc.perform(post("/corporations")
            .content(objMapper.writeValueAsString(request))
            .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(content().string("10"));
    }

}
