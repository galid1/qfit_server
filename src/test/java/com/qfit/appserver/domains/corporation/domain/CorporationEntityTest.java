package com.qfit.appserver.domains.corporation.domain;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.math.BigInteger;

@SpringBootTest
public class CorporationEntityTest {
    @Autowired
    private CorporationRepository corporationRepository;

    @Test
    public void 기업자가가치평가_테스트() throws Exception {
        //given
        CorporationEntity corporationEntity = CorporationEntity.builder()
                .corporationName("TEST")
                .password("TEST")
                .representativeInformation(RepresentativeInformation.builder().build())
                .build();

        corporationRepository.save(corporationEntity);

        //when, then
        corporationEntity.selfEvaluate(CorporationSelfEvaluateValue.builder()
                .estimatedSales(new BigInteger("100"))
                .growthRatio(BigDecimal.valueOf(110))
                .profitRatio(BigDecimal.valueOf(10))
                .discountRatio(BigDecimal.valueOf(2))
        .build());
        Assert.assertEquals(corporationEntity.getCorporationSelfEvaluateValue().getCorporationValue(),
                BigInteger.valueOf(57));

        corporationEntity.selfEvaluate(CorporationSelfEvaluateValue.builder()
                .estimatedSales(new BigInteger("100"))
                .growthRatio(BigDecimal.valueOf(-110))
                .profitRatio(BigDecimal.valueOf(10))
                .discountRatio(BigDecimal.valueOf(2))
                .build());
        Assert.assertEquals(corporationEntity.getCorporationSelfEvaluateValue().getCorporationValue(),
                BigInteger.valueOf(12));

        corporationEntity.selfEvaluate(CorporationSelfEvaluateValue.builder()
                .estimatedSales(new BigInteger("1000"))
                .growthRatio(BigDecimal.valueOf(110))
                .profitRatio(BigDecimal.valueOf(10))
                .discountRatio(BigDecimal.valueOf(2))
                .build());
        Assert.assertEquals(corporationEntity.getCorporationSelfEvaluateValue().getCorporationValue(),
                BigInteger.valueOf(573));


        corporationEntity.selfEvaluate(CorporationSelfEvaluateValue.builder()
                .estimatedSales(new BigInteger("10000"))
                .growthRatio(BigDecimal.valueOf(110))
                .profitRatio(BigDecimal.valueOf(10))
                .discountRatio(BigDecimal.valueOf(2))
                .build());
        Assert.assertEquals(corporationEntity.getCorporationSelfEvaluateValue().getCorporationValue(),
                BigInteger.valueOf(5734));


        corporationEntity.selfEvaluate(CorporationSelfEvaluateValue.builder()
                .estimatedSales(new BigInteger("100000"))
                .growthRatio(BigDecimal.valueOf(110))
                .profitRatio(BigDecimal.valueOf(10))
                .discountRatio(BigDecimal.valueOf(2))
                .build());
        Assert.assertEquals(corporationEntity.getCorporationSelfEvaluateValue().getCorporationValue(),
                BigInteger.valueOf(57336));


        corporationEntity.selfEvaluate(CorporationSelfEvaluateValue.builder()
                .estimatedSales(new BigInteger("100000000000"))
                .growthRatio(BigDecimal.valueOf(110))
                .profitRatio(BigDecimal.valueOf(10))
                .discountRatio(BigDecimal.valueOf(2))
                .build());
        Assert.assertEquals(corporationEntity.getCorporationSelfEvaluateValue().getCorporationValue(),
                new BigInteger("57336065817"));
    }
}
